import { Image } from 'src/app/interfaces/image';
import { IMAGES } from 'src/assets/mockedImages';

export function getMockedImages(): Image[] {
  return Object.values(IMAGES) as Image[];
}


