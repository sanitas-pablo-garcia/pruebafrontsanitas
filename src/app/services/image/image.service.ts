import { Injectable } from '@angular/core';
import { config } from 'src/app/config';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Image } from 'src/app/interfaces/image';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  imagesFileName: string;
  private loadedImages: Array<Image>;

  constructor(private http: HttpClient) {
    this.imagesFileName = config.imagesFileName;
  }

  public loadImages(): Observable<Image[]> {
    return this.http.get<Image[]>(this.imagesFileName)
      .pipe(
        tap(data => this.loadedImages = data),
        catchError(error => {
          return throwError(error);
        })
      );
  }
  public getImages(pageNumber: number, accumulative: boolean = false): Array<Image> {
    const start = accumulative ? 0 : (pageNumber - 1) * config.imagerPerPage;
    let end = start + config.imagerPerPage;
    if (end > this.loadedImages.length - 1) {
      end = this.loadedImages.length;
    }

    return this.loadedImages.slice(start, end);
  }

  getLoadedImages() {
    return this.loadedImages;
  }

  setLoadedImages(loadedImages) {
    this.loadedImages = loadedImages;
  }

  filterImages(searchTerm: string, pageNumber?: number) {
    if (!searchTerm) {
      return this.getImages(pageNumber ?? 1, true);
    }
    else {
      return this.loadedImages.filter(currentImage => {
        if (currentImage?.id && searchTerm) {
          return (currentImage.id.toString() === searchTerm || currentImage.text.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
        }
      });
    }
  }
}
