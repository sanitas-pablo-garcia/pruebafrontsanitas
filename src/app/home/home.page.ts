import { Component, OnInit } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';
import { Image } from '../interfaces/image';
import { ImageService } from '../services/image/image.service';
import { config } from 'src/app/config';
import { Observable, Observer } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public images: Array<Image> = [];
  public loadingImages = true;
  public logoSrc = config.appLogo;
  public maxScrollReached = false;
  public searching = false;
  public lottieOptions: AnimationOptions = {
    path: config.loadingLottie,
    autoplay: true,
    loop: true
  };
  private pageNumber = 1;

  constructor(private imageService: ImageService) { }

  doRefresh(event) {
    this.pageNumber = 1;
    this.images = [];
    this.getImages();
    event.target.complete();
  }

  ionViewWillEnter() {
    this.loadingImages = true;
    this.imageService.loadImages().subscribe(() => {
      this.loadingImages = false;
      this.getImages();
    });
  }

  getImages() {
    const newImages = this.imageService.getImages(this.pageNumber);

    newImages.forEach(image => {
      this.images.push(image as Image);
    });

    if (newImages.length < config.imagerPerPage) {
      this.maxScrollReached = true;
    }
  }

  doScroll(event) {
    this.pageNumber++;
    this.getImages();
    event.target.complete();
  }

  animationCreated(event) {
    console.log(event);
  }

  doSearch() {
    this.searching = true;
  }

  cancelSearch() {
    this.searching = false;
    this.filterImages('');
  }

  checkSearchBtnVisibility() {
    return this.images.length;
  }

  filterImages(event) {
    const searchTerm = event?.srcElement?.value ?? '';
    this.images = this.imageService.filterImages(searchTerm, this.pageNumber);
  }
}
