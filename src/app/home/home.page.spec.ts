import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, flush, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { of } from 'rxjs';
import { ImageService } from '../services/image/image.service';
import { HomePageModule } from './home.module';

/* import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
 */
import { HomePage } from './home.page';
import { getMockedImages } from './setup-test-data';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let el: DebugElement;

  const imageList = getMockedImages();
  let service: any;
  let serviceStub: Partial<ImageService>;

  serviceStub = {
    loadImages: () => of(imageList),
    getImages: () => imageList
  };

  beforeEach(waitForAsync(() => {

    TestBed.configureTestingModule({
      declarations: [HomePage],
      imports: [
        IonicModule.forRoot(),
        HomePageModule,
        HttpClientTestingModule
      ],
      providers: [
        {
          provide: ImageService,
          useValue: serviceStub,
        },
        HttpClient
      ]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(HomePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
      el = fixture.debugElement;

      service = TestBed.inject(ImageService);
    });
  }));

  it('should create the HomePage component', () => {
    expect(component).toBeTruthy();
  });

  it('should load all the images', () => {
    serviceStub.loadImages().subscribe(() => {
      expect(imageList).toBeTruthy('No images loaded');
      expect(imageList.length).toBe(20, 'Unexpected number of images loaded');
    });

  });
  it('should load all the images when loading component in ionViewWillEnter', async(() => {
    const spy = spyOn(service, 'loadImages').and.returnValue(of(imageList));
    expect(component.loadingImages).toEqual(true);
    component.ionViewWillEnter();
    expect(spy).toHaveBeenCalled();
    expect(component.loadingImages).toEqual(false);
    expect(component.images).toEqual(imageList);
  }));

  it('should display the first page of image gallery', async(() => {
    component.ionViewWillEnter();
    fixture.detectChanges();
    el = fixture.debugElement;

    fixture.whenRenderingDone().then(() => {
      const imageCards = el.queryAllNodes(By.css('.gallery-card'));
      console.log('imgs:' + imageCards);
      expect(imageCards).toBeTruthy('No .image-card was rendered');
      expect(imageCards.length).toBe(20, 'Unexpected number of images found');
    });
  }));
});
