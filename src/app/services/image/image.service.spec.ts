import { TestBed } from '@angular/core/testing';
import { ImageService } from './image.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { config } from 'src/app/config';
import { IMAGES } from 'src/assets/mockedImages';

describe('ImageService', () => {

  let service: ImageService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ImageService]
    });

    service = TestBed.inject(ImageService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load all images from JSON', () => {

    service.loadImages().subscribe(images => {
      expect(images).toBeTruthy('No images loaded');
      expect(images.length).toBe(4000, 'Incorrect number of images');

      const image = images.find(searchedImage => searchedImage.id = 1);
      expect(image.photo).toBe('https://picsum.photos/id/1/500/500');
      expect(image.text).toBe('Duis duis aute dolor cupidatat fugiat commodo. Minim incididunt ex duis irure dolor anim ipsum excepteur ut sit laborum duis.\nExcepteur fugiat consequat ut qui ea anim sit Lorem occaecat excepteur dolore et enim. Mollit in ea sunt adipisicing duis ex laboris enim in qui.');
    });

    const req = httpTestingController.expectOne(config.imagesFileName);
    expect(req.request.method).toEqual('GET');
  });

  it('should retrieve the first page of the image gallery', () => {
    const pageNumber = 1;
    service.setLoadedImages(IMAGES);
    const images = service.getImages(pageNumber);

    expect(images).toBeTruthy('No first ' + config.imagerPerPage + ' imges retrieved.');
    expect(images.length).toBe(config.imagerPerPage, 'Incorrect number of images in a page');

    const image = images.find(searchedImage => searchedImage.id === 5);

    expect(image.photo).toBe('https://picsum.photos/id/5/500/500', 'Wrong image in 5th position.');
    expect(image.text).toBe('Consequat sunt culpa nisi eiusmod velit Lorem. Excepteur aliqua nisi dolor voluptate commodo nisi eiusmod ea labore eu.\nConsectetur enim nulla ad nisi ipsum dolor laborum non veniam aliquip. Labore aute sunt aute id.');
  });

  it('should return the previously loaded images array object', () => {
    service.setLoadedImages(IMAGES);
    const loadedImages = service.getLoadedImages();
    expect(loadedImages).toBeTruthy();
    expect(loadedImages.length).toBe(20);
  });

  it('should set a JSON of images directly', () => {
    const loadedImages = service.getLoadedImages();
    expect(loadedImages).toBeFalsy();
    service.setLoadedImages(IMAGES);
    const loadedImagesAfterSet = service.getLoadedImages();
    expect(loadedImagesAfterSet).toBeTruthy();
    expect(loadedImagesAfterSet.length).toBe(20);
  });

  it('should return an image when filter by existent id', () => {
    const loadedImages = service.getLoadedImages();
    expect(loadedImages).toBeFalsy();
    service.setLoadedImages(IMAGES);
    let loadedImagesAfterFiltering = service.filterImages('1');
    expect(loadedImagesAfterFiltering).toBeTruthy();
    expect(loadedImagesAfterFiltering.length).toBe(1);

    loadedImagesAfterFiltering = service.filterImages('5555');
    expect(loadedImagesAfterFiltering).toBeTruthy();
    expect(loadedImagesAfterFiltering.length).toBe(0);
  });

  it('should return 1 image if filtering by existent id or empty array if not exists', () => {
    const loadedImages = service.getLoadedImages();
    expect(loadedImages).toBeFalsy();
    service.setLoadedImages(IMAGES);
    const loadedImagesAfterFiltering = service.filterImages('5555');
    expect(loadedImagesAfterFiltering).toBeTruthy();
    expect(loadedImagesAfterFiltering.length).toBe(0);
  });

  it('should return and array of image if filtering by a criteria that matches description or empty array when no matches found', () => {
    const loadedImages = service.getLoadedImages();
    expect(loadedImages).toBeFalsy();
    service.setLoadedImages(IMAGES);
    let loadedImagesAfterFiltering = service.filterImages('fugiat commodo');
    expect(loadedImagesAfterFiltering).toBeTruthy();
    expect(loadedImagesAfterFiltering.length).toBeGreaterThan(0);

    loadedImagesAfterFiltering = service.filterImages('fugiat_bad commodo');
    expect(loadedImagesAfterFiltering).toBeTruthy();
    expect(loadedImagesAfterFiltering.length).toBe(0);
  });
});

