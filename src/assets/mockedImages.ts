// First 27 images have been extracted from the original images.json, to make the second test
// of image.service.ts to accomplish well
export const IMAGES: any = [{
    id: 1,
    photo: 'https://picsum.photos/id/1/500/500',
    text: 'Duis duis aute dolor cupidatat fugiat commodo. Minim incididunt ex duis irure dolor anim ipsum excepteur ut sit laborum duis.\nExcepteur fugiat consequat ut qui ea anim sit Lorem occaecat excepteur dolore et enim. Mollit in ea sunt adipisicing duis ex laboris enim in qui.'
  },
  {
    id: 2,
    photo: 'https://picsum.photos/id/2/500/500',
    text: 'Consectetur voluptate ut sit id. Aute laborum consequat ut incididunt laborum eiusmod aliquip ex qui.\nLorem fugiat deserunt Lorem exercitation consectetur proident adipisicing. Reprehenderit magna occaecat laboris voluptate fugiat ex tempor adipisicing reprehenderit ad mollit.'
  }, {
    id: 3,
    photo: 'https://picsum.photos/id/3/500/500',
    text: 'Enim duis occaecat consequat incididunt id commodo sit aliqua ea sunt do consectetur id nulla. Dolor laboris quis consequat pariatur nulla magna aliqua dolore et.\nDolor in ex commodo enim aliqua proident dolor. Dolore in nostrud sint aliquip laborum.'
  },
  {
    id: 4,
    photo: 'https://picsum.photos/id/4/500/500',
    text: 'Eu quis elit quis ullamco duis nulla incididunt. Nisi et incididunt labore incididunt ea.\nEnim Lorem est adipisicing laboris ea esse cillum eu ipsum commodo ea eu consequat. Nostrud do eu ipsum minim ipsum culpa mollit nulla adipisicing incididunt.'
  }, {
    id: 5,
    photo: 'https://picsum.photos/id/5/500/500',
    text: 'Consequat sunt culpa nisi eiusmod velit Lorem. Excepteur aliqua nisi dolor voluptate commodo nisi eiusmod ea labore eu.\nConsectetur enim nulla ad nisi ipsum dolor laborum non veniam aliquip. Labore aute sunt aute id.'
  }, {
    id: 6,
    photo: 'https://picsum.photos/id/6/500/500',
    text: 'Consequat reprehenderit irure aliquip occaecat non. Anim ullamco tempor irure ex.\nCupidatat et laborum deserunt pariatur culpa ea deserunt laboris nisi. Elit pariatur excepteur eiusmod consectetur reprehenderit excepteur exercitation magna ad dolor adipisicing reprehenderit aliqua.'
  }, {
    id: 7,
    photo: 'https://picsum.photos/id/7/500/500',
    text: 'Eiusmod ad culpa ea ea proident incididunt proident excepteur pariatur Lorem. Amet enim veniam incididunt fugiat in pariatur.\nIn id fugiat in et deserunt eiusmod excepteur labore exercitation magna irure magna aliqua aliquip. Cillum amet fugiat proident irure pariatur aute eiusmod eiusmod elit officia.'
  }, {
    id: 8,
    photo: 'https://picsum.photos/id/8/500/500',
    text: 'In eiusmod et voluptate laboris ullamco fugiat deserunt non nulla ipsum. Mollit non non est elit nisi consectetur aute veniam deserunt.\nCupidatat in eiusmod do cupidatat laboris consequat. Proident dolor aliquip proident commodo dolore nisi amet nostrud eu consectetur ipsum esse.'
  }, {
    id: 9,
    photo: 'https://picsum.photos/id/9/500/500',
    text: 'Officia cillum nulla non velit ullamco proident consequat tempor cillum. Amet id exercitation est minim Lorem et.\nCommodo reprehenderit magna ullamco dolor esse officia elit mollit. Minim esse aliquip commodo sit Lorem irure enim ut magna consequat.'
  }, {
    id: 10,
    photo: 'https://picsum.photos/id/10/500/500',
    text: 'Nulla cillum est exercitation deserunt adipisicing elit esse proident laborum exercitation est aute commodo. Est Lorem quis laboris magna enim sint veniam laborum.\nSunt ad magna enim veniam officia nisi. Culpa amet qui aliqua do non deserunt quis sunt.'
  }, {
    id: 11,
    photo: 'https://picsum.photos/id/11/500/500',
    text: 'Elit non commodo amet proident ut in ipsum adipisicing officia occaecat sit nisi. Labore nisi enim fugiat ipsum sint velit.\nSint elit aute cillum laboris. Lorem minim voluptate cillum minim aliquip deserunt irure nisi eiusmod non.'
  }, {
    id: 12,
    photo: 'https://picsum.photos/id/12/500/500',
    text: 'Deserunt exercitation do excepteur ullamco duis. Labore proident ad eu amet in nisi ullamco non ipsum minim.\nOccaecat irure enim mollit amet nulla aute voluptate aliquip. Quis proident dolore commodo anim.'
  }, {
    id: 13,
    photo: 'https://picsum.photos/id/13/500/500',
    text: 'Cupidatat adipisicing adipisicing culpa sunt sit. Eu veniam in Lorem dolore nulla.\nEst dolor incididunt veniam qui ea nisi sit Lorem. Proident culpa consequat aliqua eu occaecat elit aliqua magna.'
  }, {
    id: 14,
    photo: 'https://picsum.photos/id/14/500/500',
    text: 'Quis nostrud laborum nostrud et nulla et Lorem aliqua. Sunt occaecat ipsum dolor enim sunt ut amet dolore et pariatur exercitation dolor exercitation ad.\nMagna id esse Lorem dolore nisi aute eiusmod aliqua pariatur nisi officia. Minim aliquip occaecat ea duis consequat ullamco qui non Lorem non proident aliqua do.'
  }, {
    id: 15,
    photo: 'https://picsum.photos/id/15/500/500',
    text: 'Officia qui sit reprehenderit tempor minim aliquip sint enim labore. Esse cillum amet anim cupidatat sint dolore nostrud aliquip sint.\nIrure incididunt minim mollit ipsum consequat veniam magna sint laboris incididunt duis quis veniam. Proident deserunt culpa ipsum exercitation.'
  }, {
    id: 16,
    photo: 'https://picsum.photos/id/16/500/500',
    text: 'Veniam proident deserunt aliqua dolor velit aute ut ea enim nostrud in. In anim non Lorem quis anim quis ex reprehenderit do.\nAdipisicing culpa eiusmod nisi pariatur ad. Adipisicing proident ea fugiat sunt.'
  }, {
    id: 17,
    photo: 'https://picsum.photos/id/17/500/500',
    text: 'Mollit ad tempor velit ut pariatur dolor officia. Qui laborum magna fugiat pariatur occaecat cillum eiusmod ea laborum amet reprehenderit quis consectetur.\nFugiat incididunt consectetur anim ipsum incididunt. Exercitation sint irure et dolore Lorem et elit veniam enim tempor non officia laborum labore.'
  }, {
    id: 18,
    photo: 'https://picsum.photos/id/18/500/500',
    text: 'Id id velit incididunt exercitation cillum pariatur duis. Voluptate ea magna et fugiat mollit irure ullamco aute.\nOfficia occaecat commodo aute dolor tempor aute fugiat labore veniam fugiat ullamco consequat minim labore. Qui tempor consequat non et.'
  }, {
    id: 19,
    photo: 'https://picsum.photos/id/19/500/500',
    text: 'Do aliquip est ad commodo nisi commodo. Nulla ex et ex officia dolore.\nEx aliqua duis velit amet enim laboris aliqua non dolor sit laborum. Excepteur laborum adipisicing irure minim.'
  }, {
    id: 20,
    photo: 'https://picsum.photos/id/20/500/500',
    text: 'Excepteur adipisicing nisi excepteur enim. Labore aute laboris elit excepteur mollit deserunt eiusmod proident sit officia velit non esse proident.\nPariatur laboris ad exercitation exercitation voluptate elit quis irure est consequat occaecat. Nisi aliqua cillum commodo id ad aliqua id.'
  }
];
