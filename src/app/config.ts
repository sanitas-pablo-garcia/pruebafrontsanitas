/**
 * config template
 */
 export const config = {
    appLogo: 'assets/logo.png',
    signText: 'Pablo García Ojeda',
    loadingLottie: 'assets/lotties/image-loading.json',
    imagesFileName: '../../assets/images.json',
    imagerPerPage: 6
  };
